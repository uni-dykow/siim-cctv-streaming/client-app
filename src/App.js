import { Tab } from "@headlessui/react";
import { useState } from "react";
import { Header } from "./Components/Header";
import { Settings } from "./Components/Settings";
import { Streams } from "./Components/Streams";

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export default function App() {
  const [pages] = useState([
    {
      id: 0,
      name: "Settings",
      component: <Settings/>
    },
    {
      id: 1,
      name: "Streams",
      component: <Streams/>
    }
  ])

  return (
    <>
      <Header/>
      <Tab.Group>
        <Tab.List className={"container mx-auto mt-6 rounded-xl ring-offset-teal-100 ring-opacity-60 grid grid-cols-2 place-items-center"}>
        {
        pages.map((page) => (
          <Tab key={page.id} className={({ selected }) =>
          classNames("my-2 w-5/6 h-5/6 font-semibold text-lg ring-opacity-60 rounded-xl ring-offset-2 ring-offset-teal-400 ",selected ? "bg-teal-500 text-white" : "bg-white text-teal-500 hover:bg-gray-200 hover:text-teal-500")}>{page.name}</Tab>
        ))
      }
        </Tab.List>
      
      <Tab.Panels className="my-2 container mx-auto grid place-items-center">
        {
          pages.map((page) => (
            <Tab.Panel key={page.id} className={classNames(
              'rounded-xl bg-white p-3')}>
                {page.component}
              </Tab.Panel>
          ))
        }
      </Tab.Panels>
    </Tab.Group>
    </>
  )
}

