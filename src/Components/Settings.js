import axios from "axios"
import { useEffect, useState } from "react"
import AddCameraModal from "./Modals/AddCameraModal"
import BaseSwitch from "./Switches/BaseSwitch"
import EditCameraModal from "./Modals/EditCameraModal"
import DeleteCameraModal from "./Modals/DeleteCameraModal"

export const Settings = () => {
    const [streamConfigs, setStreamConfigs] = useState([])
    const [url, setUrl] = useState("")
    const [enabled, setEnabled] = useState(false)

    useEffect(() => {
        async function fetchData() {
            try {
                const res = await axios.get(process.env.REACT_APP_API_URL)
                setStreamConfigs(res.data.config)
            } catch (error) {
                console.error(error)
                alert("There are no cameras availabled.")
            } finally {
                return []
            }
        }
        fetchData()
    }, [])

    const postConfiguration = async () => {
        const config = streamConfigs
        await axios.post(process.env.REACT_APP_API_URL, { config }).then(res => {
            alert("New configuration has been sent")
        })
    }
    

    const handleCameraState = (id) => {
        let tempArray = [...streamConfigs]
        tempArray[id].enabled = !tempArray[id].enabled
        setStreamConfigs(tempArray)
    }

    const handleCameraAdd = () => {
        setStreamConfigs([
            ...streamConfigs,
            {
                id: streamConfigs.length,
                enabled: enabled,
                url: url
            }
        ])
        setEnabled(false)
        setUrl("")
    }

    const handleCameraEdit = (config) => {
        let tempArray = [...streamConfigs]
        tempArray[config.id] = config
        setStreamConfigs(tempArray)
    }

    const handleCameraDelete = (id) => {
        let tempArray = [...streamConfigs]
        tempArray.splice(id, 1)
        setStreamConfigs(tempArray)
    }

    const generateCameraSwitches = () => {
        if (streamConfigs.length > 0) {
            return (
                streamConfigs.map((config) => (
                    <div key={config.id}>
                        <div>
                        <BaseSwitch desc={`Camera's URL: ${config.url}`} enabled={config.enabled} handleChange={() => handleCameraState(config.id)} modal={false} />
                        </div>
                        <div className="grid grid-cols-2 place-items-center">
                            <EditCameraModal config={config} handleCameraEdit={handleCameraEdit} />
                            <DeleteCameraModal url={config.url} id={config.id} handleCameraDelete={() => handleCameraDelete(config.id)} />
                        </div>
                    </div>
                    
                ))
            )
        } else {
            return (
                <>
                    There are no cameras availabled
                </>
            )
        }
    }

    return (
        <div className={"container mx-auto my-10"}>
            {generateCameraSwitches()}
            <div className="grid grid-cols-2 gap-4 place-items-center w-full">
                <AddCameraModal handleCameraAdd={handleCameraAdd} setUrl={setUrl} url={url} enabled={enabled} setEnabled={setEnabled} />
                <button className="w-4/5 bg-teal-500 hover:bg-teal-700 ml-10 my-10 text-white font-bold py-2 px-4 rounded" onClick={postConfiguration}>
                    Set Configuration
                </button>
            </div>
        </div>
    )
}