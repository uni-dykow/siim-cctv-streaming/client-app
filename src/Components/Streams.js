import ReactHlsPlayer from "react-hls-player/dist"

export const Streams = () => {
    return(
        <>
            <div className={"container mx-auto "}>
                <ReactHlsPlayer src={process.env.REACT_APP_STREAM_URL} autoPlay={false} controls={true} width="100%" height="auto"/>
            </div>
        </>
    )
}