import BaseSwitch from "./BaseSwitch";


export default function CameraSwitch(props) {

    const handleDeleteItem = () => {
        alert("Item will be deleted")
    }

    const handleEditItem = () => {
        alert("Item will be edited")
    }

    return (
        <div className="grid grid-cols-2 place-items-center gap-4 ">
            <BaseSwitch desc={props.desc} enabled={props.config.enabled} handleChange={() => props.handleCameraState(props.config.id)} modal={false} />
            <div className="grid grid-cols-2 gap-4">
                <button>
                    <label onClick={handleEditItem}>
                        Edit
                    </label>
                </button>
                <button onClick={handleDeleteItem}>
                    <label className="">
                        Delete
                    </label>
                </button>
            </div>
        </div>
    )
}