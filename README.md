# client-app

Client web application that provides settings of cameras and allows to watch streams processed by server.

## Getting started

To configure connection with API create .env file and set following environmental variable:

*REACT_APP_API_URL* - URL to API's endpoint that provides streams settings

*REACT_APP_STREAM_URL* - URL to endpoint that tramsmits processed stream

To run project type npm install (installing dependencies) and next npm start in console window.