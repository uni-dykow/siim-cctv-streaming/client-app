from urllib import request
from flask import Flask, request
from flask_cors import CORS
import json

app = Flask(__name__)
CORS(app)

@app.route("/", methods=['POST', 'GET'])
def index():
    if request.method == 'GET':
        return json.dumps({
            'config': [
                {
                    'id': 0,
                    'url': "host:port/garage",
                    'enabled': False
                },
                {
                    'id': 1,
                    'url': "host:port/kitchen",
                    'enabled': False
                },
                {
                    'id': 2,
                    'url': "host:port/hall",
                    'enabled': False
                }
            ]
        })
    else:
        print(request.json)
        return "Hello"

app.run(host="localhost", port=5001, debug=True)